﻿using System.Threading.Tasks;
using MassTransit;
using MassTransit.Definition;
using MassTransit.RabbitMqTransport;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using MKOPA.Components;

namespace MKOPA.Services
{
    class Program
    {
      
        static async Task Main(string[] args)
        {

            var builder = new HostBuilder()
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    config.AddJsonFile("appsettings.json", true);
                    config.AddEnvironmentVariables();

                    if (args != null)
                        config.AddCommandLine(args);
                })
                .ConfigureServices((hostContext, services) =>
                {
                    /*
                    _module = new DependencyTrackingTelemetryModule();
                    _module.IncludeDiagnosticSourceActivities.Add("MassTransit");

                    TelemetryConfiguration configuration = TelemetryConfiguration.CreateDefault();
                    configuration.InstrumentationKey = "6b4c6c82-3250-4170-97d3-245ee1449278";
                    configuration.TelemetryInitializers.Add(new HttpDependenciesParsingTelemetryInitializer());
                    
                    _telemetryClient = new TelemetryClient(configuration);

                    _module.Initialize(configuration);

                    services.AddScoped<AcceptOrderActivity>();

                    services.AddScoped<RoutingSlipBatchEventConsumer>();
                   */
                    services.TryAddSingleton(KebabCaseEndpointNameFormatter.Instance);
                    services.AddMassTransit(cfg =>
                    {
                        cfg.AddConsumersFromNamespaceContaining<OutgoingSmsSaga>();
                        cfg.AddActivitiesFromNamespaceContaining<ForwarderActivity>();

                        /*
                        cfg.AddSagaDbContext<YourDbContext>(x => x.UseSql())
                        cfg.AddSaga<Instance>()
                            .UseEntityFramework(repo => 
                            {
                                repo.UseContext<YourDbContext>();
                                repo.ConcurrencyHandlingType = Pessimistic / Optimistic;
                            });
                        */
                        cfg.UsingRabbitMq(ConfigureBus);
                    });

                   // services.AddHostedService<MassTransitConsoleHostedService>();
                })
                .ConfigureLogging((hostingContext, logging) =>
                {
                   // logging.AddSerilog(dispose: true);
                    logging.AddConfiguration(hostingContext.Configuration.GetSection("Logging"));
                });

            /*
            if (isService)
                await builder.UseWindowsService().Build().RunAsync();
            else
                await builder.RunConsoleAsync();
           */
            await builder.RunConsoleAsync();
          //  _telemetryClient?.Flush();
          //  _module?.Dispose();

          //  Log.CloseAndFlush();
        }

        static void ConfigureBus(IBusRegistrationContext context, IRabbitMqBusFactoryConfigurator configurator)
        {
           

            configurator.ConfigureEndpoints(context);
        }
    }
}