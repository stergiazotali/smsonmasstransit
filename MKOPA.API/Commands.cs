using System;

namespace MKOPA.API
{
    public static class Commands
    {
        public static class V1
        {
            public class SendSms
            {
                public Guid MessageId { get; set; }
                public Guid Customer { get; set; }
                public string PhoneNumber { get; set; }
                public string SendText { get; set; }
            } 
        }
    }
}