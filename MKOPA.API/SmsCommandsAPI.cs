using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace MKOPA.API
{
    [ApiController, Route("api/ad"), Authorize]
    public class SmsCommandsApi : CommandApi
    {
        readonly ISendEndpointProvider _sendEndpointProvider;
        
        public SmsCommandsApi(
            Publisher applicationService,
            ILoggerFactory loggerFactory, ISendEndpointProvider sendEndpointProvider)
            : base(applicationService, loggerFactory)
        {
            _sendEndpointProvider = sendEndpointProvider;
        }

        // 1 way
        /*
        [HttpPost]
        public Task<IActionResult> Post(Commands.V1.SendSms command)
            => HandleCommand(command);
        */
        
        [HttpPut]
        public async Task<IActionResult> Put(Commands.V1.SendSms command)
        {
            var endpoint = await _sendEndpointProvider.GetSendEndpoint(new Uri("queue:sent-sms"));

            await endpoint.Send<Commands.V1.SendSms>(command);

            return Accepted();
        }
    }
}