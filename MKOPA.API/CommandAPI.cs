using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using MassTransit;
using Microsoft.Extensions.Logging;

namespace MKOPA.API
{
    public abstract class CommandApi : ControllerBase
    {
        readonly ILogger _log;

        protected CommandApi(
            Publisher applicationService,
            ILoggerFactory loggerFactory)
        {
            _log = loggerFactory.CreateLogger(GetType());
            Service = applicationService;
        }

        Publisher Service { get; }

        protected async Task<IActionResult> HandleCommand<TCommand>(
            TCommand command)
        {
            try
            {
                await Service.Publish(command);
                return new OkResult();
            }
            catch (Exception e)
            {
                _log.LogError(e, "Error handling the command");

                return new BadRequestObjectResult(
                    new
                    {
                        error = e.Message, 
                        stackTrace = e.StackTrace
                    }
                );
            }
        }

        protected Guid GetUserId() => Guid.Parse(User.Identity.Name);
    }

    public class Publisher
    {
        private readonly IPublishEndpoint _endpoint;
        
        public Publisher(IPublishEndpoint endpoint) => _endpoint = endpoint;

        public async Task Publish(object command)
        {
            await _endpoint.Publish(command);
        }
    }
}