using System;
using System.Threading.Tasks;
using MassTransit;
using MassTransit.Courier;
using MassTransit.Courier.Contracts;
using MassTransit.Saga;

namespace MKOPA.Components
{
    public interface SubmitSms: 
        CorrelatedBy<Guid>
    {
        Guid MessageId { get; set; }
        DateTime SentDate { get; }
        string PhoneNumber { get; }
        string Text { get; }
    }

    public class OutgoingSmsSaga:
        ISaga,
        InitiatedBy<SubmitSms>
    {
        public Guid CorrelationId { get; set; }
        public DateTime SentDate { get; set; }
        public string PhoneNumber { get; set; }
        public string Text { get; set; }
        
        public async Task Consume(ConsumeContext<SubmitSms> context)
        {
            SentDate = context.Message.SentDate;
            PhoneNumber = context.Message.PhoneNumber;
            Text = context.Message.Text;
            
            var builder = new RoutingSlipBuilder(NewId.NextGuid());

            builder.AddActivity("ForwarderActivity", new Uri("queue:sms-outgoing-deliveries"), new
            {
                PhoneNumber = PhoneNumber,
                Text = Text
            });

            builder.AddVariable("MessageId", context.Message.MessageId);

            /*
            await builder.AddSubscription(context.SourceAddress,
                RoutingSlipEvents.Faulted | RoutingSlipEvents.Supplemental,
                RoutingSlipEventContents.None, x => x.Send<OrderFulfillmentFaulted>(new {context.Message.MessageId}));

            await builder.AddSubscription(context.SourceAddress,
                RoutingSlipEvents.Completed | RoutingSlipEvents.Supplemental,
                RoutingSlipEventContents.None, x => x.Send<OrderFulfillmentCompleted>(new {context.Message.OrderId}));
            */
            var routingSlip = builder.Build();

            await context.Execute(routingSlip);
        }
    }
}