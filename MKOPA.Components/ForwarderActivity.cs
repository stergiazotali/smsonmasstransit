using System.Threading.Tasks;
using MassTransit.Courier;

namespace MKOPA.Components
{
    public class ForwarderActivity: IExecuteActivity<SmsArguments>
    {
        public async Task<MassTransit.Courier.ExecutionResult> Execute(ExecuteContext<SmsArguments> execution)
        {
            var args = execution.Arguments;
          
            //SMS API Integration

            return execution.Completed();
        }
        
        public async Task<MassTransit.Courier.CompensationResult> Compensate(CompensateContext<SmsArguments> context)
        {
           // DownloadImageLog log = compensation.Log;
           // File.Delete(log.ImageSavePath);
           // Can we publish to failure queue 
            return context.Compensated();
        }
    }
}